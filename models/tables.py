# -*- coding: utf-8 -*-
from datetime import datetime
import os
'''Table auth with auth_user is declared in  db.py.
Note: if we want users to authenticate using username then username=True. Also I have changed signature to True to get timestamps

See chapter 9 Access Control for auth_user

'''
'''
from gluon.tools import Auth

auth = Auth(db)
auth.settings.extra_fields['auth_user']= [
  Field('phone_number', 'integer', required=False),
  Field('picture', 'upload'),
  #Field('average_rate', writable=False)
  ]
## before auth.define_tables(username=True)

'''

########################################################################
#
#  This is all the mostly all the databases were gonna use in the webapp
#  so in our webapp we're gonna have a rating system attached to a user 1 - 5
#  then were gonna have a posts database to store information about them
#since each post is associated with a user and our webapp depends on a user were gonna have profiles
# NOTICE that the photos field is of type upload, this allows us to associate file(s) with a post
db.define_table('posts',
                Field('user_id', db.auth_user, default= auth.user_id),
                Field('name', required = True),
                Field('description', 'text'),
                Field('created_on', 'datetime', default = datetime.utcnow()),
                Field('num_of_photos'),
                Field ('post_id'),
                Field('is_edit', 'boolean', default= False),
                Field('category', 'string'),
                Field('cover_photo','upload', default = os.path.join(request.folder, 'static', 'images', 'default_coverphoto.jpg')),
                Field('price','string'),
                )


db.define_table('product_image',
                Field('picture','upload'),
                Field('photo_name'),
                Field('post_id'),
                Field('created_on', default = datetime.utcnow()),
                format='%(post)s',

                )

#we could delete this
def get_author(id):
    user = db.auth_user(id)
    return A('%(first_name)s %(last_name)s' % user, _href=URL('profile', args=user.id))
