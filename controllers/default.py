# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

from gluon import utils as gluon_utils
import json
import time
import gluon.contrib.simplejson
import os


def index():
    post_id = gluon_utils.web2py_uuid()
    photos = db().select(db.product_image.ALL, orderby=db.product_image.created_on, limitby=(0, 3))
    list = []
    for row in photos:
        picture = {'photo': row.picture}
        list.append(picture)
    return dict(post_id=post_id, photos=list)


def posts():
    # print request.args(0)
    post_id = request.args(0)

    return dict(post_id=post_id)


def get_post():
    post_id = request.vars.post_id
    pictures = db(db.product_image.post_id == post_id).select(db.product_image.ALL)
    post = db(db.posts.post_id == post_id).select(db.posts.ALL).first()
    pic_list = []
    post_info = []
    for row in pictures:
        pic = {'picture': row.picture}
        pic_list.append(pic)
    p_data = {'category': post.category,
              'user_id': post.user_id,
              'description': post.description,
              'price': post.price,
              'cover_photo': post.cover_photo,
              'created_on': post.created_on,
              'name': post.name,
              }
    post_info.append(p_data)

    user = db(db.auth_user.id == post.user_id).select(db.auth_user.ALL).first()

    poster_list = []
    poster = {'first_name': user.first_name,
              'last_name': user.last_name,
              'phone': user.phone,
              'email': user.email,
              }
    poster_list.append(poster)

    return response.json(dict(pic_list=pic_list, post_info=post_info, poster_list=poster_list))


def load_posts():
    # this function will load all initial posts
    # we will do pagination like the index above
    # use limitby
    #db.posts.truncate()
    #db.product_image.truncate()
    p_list = db().select(db.posts.ALL, orderby=~db.posts.created_on)
    photo_list = db().select(db.product_image.ALL)
    users = db().select(db.auth_user.ALL)
    print(users)

    # do something for the thumbnail photo

    list = []
    for row in p_list:
        cover_photo = db(db.product_image.post_id == row.post_id).select(db.product_image.ALL,
                                                                         orderby=db.product_image.created_on).first()

        post = {'post_id': row.post_id,
                'name': row.name,
                'is_edit': row.is_edit,
                'category': row.category,
                'desc': row.description,
                'photos': row.cover_photo,
                'created_on': row.created_on,
                'user_id': row.user_id,
                'cover_photo': cover_photo.picture,
                'price': row.price,
                }
        print (cover_photo.picture)
        list.append(post)

    list_photo = []
    for row in photo_list:
        photo = {'picture': row.picture,
                 'post_id': row.post_id,

                 }
        list_photo.append(photo)

    print (photo_list)
    print (p_list)
    return response.json(dict(post_list=list, photo_list=list_photo))


def show_posts():
    # print ("passed in" , type(request.vars.userid))
    curr_user = request.vars.userid
    post_list = db(db.posts.user_id == curr_user).select(db.posts.ALL, orderby=~db.posts.created_on)
    list = []
    for row in post_list:
        # print("hereinloop")
        cover_photo = db(db.product_image.post_id == row.post_id).select(db.product_image.ALL,
                                                                         orderby=db.product_image.created_on).first()

        db.posts.update_or_insert((db.posts.post_id == row.post_id),
                                  cover_photo=cover_photo.picture)
        row.cover_photo = cover_photo

        post = {'post_id': row.post_id,
                'name': row.name,
                'is_edit': row.is_edit,
                'category': row.category,
                'desc': row.description,
                'photos': row.cover_photo,
                'created_on': row.created_on,
                'user_id': row.user_id,
                'cover_photo': cover_photo.picture,
                'price': row.price
                }
        list.append(post)

    return response.json(dict(post_list=list))


def add_post():
    file_name = request.vars.file.filename
    print ("what", file_name)
    file = request.vars.file
    post_name = request.vars.file.name
    db.posts.update_or_insert((db.posts.post_id == request.vars["post_id"]),
                              name=request.vars["post_name"],
                              description=request.vars["post_desc"],
                              post_id=request.vars["post_id"],
                              category=request.vars["post_cate"],
                              price=request.vars["post_price"],
                              )
    db.product_image.insert(post_id=request.vars["post_id"], picture=file, photo_name=file_name)

    # p_list = db().select(db.posts.ALL)
    # db.product_image.truncate()
    photo_list = db().select(db.posts.ALL)
    for row in photo_list:
        print ("row", row.user_id)
    list = "ok"
    # upload to server and send back the id of photo incase we need to remove it
    return response.json(dict(post_list=list))


def edit_post():
    print request.vars.file.filename
    file = request.vars.file


def update_post():
    print("post id", request.vars.post_id)
    new_name = request.vars.name
    new_desc = request.vars.desc
    new_cate = request.vars.cate
    new_price = request.vars.price
    db.posts.update_or_insert(db.posts.post_id == request.vars.post_id,
                              name=new_name,
                              description=new_desc,
                              category=new_cate,
                              price=new_price,
                              )

    return "ok"


def your_profile():
    print ("your profile", request.args(0))
    user = db(db.auth_user.id == request.args(0)).select(db.auth_user.ALL).first()
    profile = []
    user_info = {'first_name': user.first_name,
                 'last_name': user.last_name,
                 'phone': user.phone,
                 'email': user.email,
                 'photo': user.photo
                 }
    profile.append(user_info)
    print(user)
    return dict(user_id=request.args(0), profile=profile)


def get_pictures():
    post_id = request.vars.postid
    print post_id
    images = db(db.product_image.post_id == post_id).select(db.product_image.ALL, orderby=db.product_image.created_on)
    photos = []
    for row in images:
        post_pic = row.picture
        photos.append(post_pic)

    print photos
    return response.json(dict(photos=photos))


def load_posts_user():
    print("nvm")

    return dict()


def delete_posts():
    print(request.vars.post_id)
    post_id = request.vars.post_id
    counter = db(db.product_image.post_id == post_id).count()
    print (counter)
    for x in range(counter):
        print("deleting photo", x)
        db(db.product_image.post_id == post_id).delete()

    db(db.posts.post_id == post_id).delete()
    return "deleted"


def delete_pic():
    pic_name = request.vars.to_delete
    the_id = request.vars.post_id
    print("Picture to be deleted", pic_name)
    print("Post id ", the_id)

    db(db.product_image.picture == pic_name).delete()
    curr_post = db(db.posts.post_id == the_id).select(db.posts.ALL).first()
    print("curr post", curr_post)
    if curr_post.cover_photo is None:
        print("is None")
        next_photo = db(db.product_image.post_id == the_id).select(db.product_image.ALL,
                                                                   orderby=db.product_image.created_on).first()
        db.posts.update_or_insert((db.posts.post_id == the_id),
                                  cover_photo=next_photo.picture)
    return "ok"


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_login()
def api():
    """
    this is example of API with access control
    WEB2PY provides Hypermedia API (Collection+JSON) Experimental
    """
    from gluon.contrib.hypermedia import Collection
    rules = {
        '<tablename>': {'GET': {}, 'POST': {}, 'PUT': {}, 'DELETE': {}},
    }
    return Collection(db).process(request, response, rules)
